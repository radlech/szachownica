//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "NowaGra.h"
#include "Widok.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOknoNowaGra *OknoNowaGra;
//---------------------------------------------------------------------------
__fastcall TOknoNowaGra::TOknoNowaGra(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TOknoNowaGra::ButtonNowaGraClick(TObject *Sender)
{
  if (ComboBoxBialy->Text == "" || ComboBoxCzarny->Text == "")
    ShowMessage("Nie wybrano graczy.");
  else if (ComboBoxBialy->Text == ComboBoxCzarny->Text)
    ShowMessage("Wybierz dwoch roznych graczy.");
  else
  {
    OknoGlowne -> utworzGre(
      Gracz(ComboBoxBialy->Text.c_str()),
      Gracz(ComboBoxCzarny->Text.c_str())
    );
    OknoNowaGra -> Close();
  }
}
//---------------------------------------------------------------------------
void TOknoNowaGra::wczytajGraczy(std::list<Gracz> g)
{
  gracze = g;
  std::list<Gracz>::iterator it;
  ComboBoxBialy -> Items -> Clear();
  ComboBoxCzarny -> Items -> Clear();
  for (it = gracze.begin(); it != gracze.end(); ++it)
  {
    ComboBoxBialy -> Items -> Add(it -> pobierzImie().c_str());
    ComboBoxCzarny -> Items -> Add(it -> pobierzImie().c_str());
  }
}
//---------------------------------------------------------------------------

