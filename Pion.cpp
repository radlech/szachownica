//---------------------------------------------------------------------------
#pragma hdrstop
#include "Pion.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
Pion::Pion(Kolor k)
{
  kolor = k;
}
//---------------------------------------------------------------------------
bool Pion::walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza)
{
  Figura *f = plansza[skad];
  if ( f->sprawdzKolor() == BIALY && plansza.find(dokad) == plansza.end() )
  {
    if ( dokad == Pole(skad.x, skad.y+1) )
      return true;
    else if ( skad.y == 2 && dokad == Pole(skad.x, skad.y+2) )
      return true;
  }
  else if ( f->sprawdzKolor() == BIALY && plansza.find(dokad) != plansza.end() )
  {
    if ( dokad == Pole(skad.x-1, skad.y+1) || dokad == Pole(skad.x+1, skad.y+1) )
      return true;
  }
  else if ( f->sprawdzKolor() == CZARNY && plansza.find(dokad) == plansza.end() )
  {
    if ( dokad == Pole(skad.x, skad.y-1) )
      return true;
    else if ( skad.y == 7 && dokad == Pole(skad.x, skad.y-2) )
      return true;
  }
  else if ( f->sprawdzKolor() == CZARNY && plansza.find(dokad) != plansza.end() )
  {
    if ( dokad == Pole(skad.x-1, skad.y-1) || dokad == Pole(skad.x+1, skad.y-1) )
      return true;
  }
  return false;
}
//---------------------------------------------------------------------------
std::string Pion::przedstawSie()
{
  return "Pion";
}
//---------------------------------------------------------------------------
