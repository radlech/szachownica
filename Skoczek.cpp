//---------------------------------------------------------------------------
#pragma hdrstop
#include "Skoczek.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
Skoczek::Skoczek(Kolor k)
{
  kolor = k;
}
//---------------------------------------------------------------------------
bool Skoczek::walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza)
{
  if ( std::abs(skad.x - dokad.x) == 1 && std::abs(skad.y - dokad.y) == 2 )
    return true;
  else if ( std::abs(skad.x - dokad.x) == 2 && std::abs(skad.y - dokad.y) == 1 )
    return true;
  return false;
}
//---------------------------------------------------------------------------
std::string Skoczek::przedstawSie()
{
  return "Skoczek";
}
//---------------------------------------------------------------------------
