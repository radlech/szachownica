//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("Widok.cpp", OknoGlowne);
USEFORM("Autor.cpp", OknoAutor);
USEFORM("NowaGra.cpp", OknoNowaGra);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TOknoGlowne), &OknoGlowne);
                 Application->CreateForm(__classid(TOknoAutor), &OknoAutor);
                 Application->CreateForm(__classid(TOknoNowaGra), &OknoNowaGra);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
