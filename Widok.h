//---------------------------------------------------------------------------
#ifndef WidokH
#define WidokH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ExtCtrls.hpp>
#include <Gra.h>
#include <Ranking.h>
#include <jpeg.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
/*!
  Klasa widoku odpowiedzialna za glowne okno gry.
*/
class TOknoGlowne : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MenuGlowne;
        TMenuItem *d1;
        TMenuItem *Pomoc1;
        TMenuItem *Nowagra1;
        TMenuItem *Wczytajgre1;
        TMenuItem *Zapiszgre1;
        TMenuItem *Zasadygry1;
        TMenuItem *Autor1;
        TMenuItem *Zakoncz1;
        TListBox *RankingListBox;
        TEdit *NowyGraczInput;
        TLabel *NowyGraczLabel;
        TButton *NowyGraczButton;
        TLabel *RankingLabel;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *LabelA;
        TLabel *LabelB;
        TLabel *LabelC;
        TLabel *LabelD;
        TLabel *LabelE;
        TLabel *LabelF;
        TLabel *LabelG;
        TLabel *LabelH;
        TShape *KogoRuchShape;
        TLabel *KogoRuchLabel;
        TOpenDialog *WczytajGreDialog;
        TSaveDialog *ZapiszGreDialog;
        TStaticText *GraczBialyLabel;
        TStaticText *GraczCzarnyLabel;
        TShape *GraczBialyShape;
        TShape *GraczCzarnyShape;
        TImage *X1Y8;
        TImage *X2Y8;
        TImage *X3Y8;
        TImage *X4Y8;
        TImage *X5Y8;
        TImage *X6Y8;
        TImage *X7Y8;
        TImage *X8Y8;
        TImage *X1Y7;
        TImage *X2Y7;
        TImage *X3Y7;
        TImage *X4Y7;
        TImage *X5Y7;
        TImage *X6Y7;
        TImage *X7Y7;
        TImage *X8Y7;
        TImage *X1Y6;
        TImage *X2Y6;
        TImage *X3Y6;
        TImage *X4Y6;
        TImage *X5Y6;
        TImage *X6Y6;
        TImage *X7Y6;
        TImage *X8Y6;
        TImage *X1Y5;
        TImage *X2Y5;
        TImage *X3Y5;
        TImage *X4Y5;
        TImage *X5Y5;
        TImage *X6Y5;
        TImage *X7Y5;
        TImage *X8Y5;
        TImage *X1Y4;
        TImage *X2Y4;
        TImage *X3Y4;
        TImage *X4Y4;
        TImage *X5Y4;
        TImage *X6Y4;
        TImage *X7Y4;
        TImage *X8Y4;
        TImage *X1Y3;
        TImage *X2Y3;
        TImage *X3Y3;
        TImage *X4Y3;
        TImage *X5Y3;
        TImage *X6Y3;
        TImage *X7Y3;
        TImage *X8Y3;
        TImage *X1Y2;
        TImage *X2Y2;
        TImage *X3Y2;
        TImage *X4Y2;
        TImage *X5Y2;
        TImage *X6Y2;
        TImage *X7Y2;
        TImage *X8Y2;
        TImage *X1Y1;
        TImage *X2Y1;
        TImage *X3Y1;
        TImage *X4Y1;
        TImage *X5Y1;
        TImage *X6Y1;
        TImage *X7Y1;
        TImage *X8Y1;
        TImage *ImageMat;
        void __fastcall NowyGraczButtonClick(TObject *Sender);
        void __fastcall Wczytajgre1Click(TObject *Sender);
        void __fastcall Zapiszgre1Click(TObject *Sender);
        void __fastcall Zakoncz1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Autor1Click(TObject *Sender);
        void __fastcall Zasadygry1Click(TObject *Sender);
        void __fastcall Nowagra1Click(TObject *Sender);
        void __fastcall X1Y1Click(TObject *Sender);
        void __fastcall X2Y1Click(TObject *Sender);
        void __fastcall X3Y1Click(TObject *Sender);
        void __fastcall X4Y1Click(TObject *Sender);
        void __fastcall X5Y1Click(TObject *Sender);
        void __fastcall X6Y1Click(TObject *Sender);
        void __fastcall X7Y1Click(TObject *Sender);
        void __fastcall X8Y1Click(TObject *Sender);
        void __fastcall X1Y2Click(TObject *Sender);
        void __fastcall X2Y2Click(TObject *Sender);
        void __fastcall X3Y2Click(TObject *Sender);
        void __fastcall X4Y2Click(TObject *Sender);
        void __fastcall X5Y2Click(TObject *Sender);
        void __fastcall X6Y2Click(TObject *Sender);
        void __fastcall X7Y2Click(TObject *Sender);
        void __fastcall X8Y2Click(TObject *Sender);
        void __fastcall X1Y3Click(TObject *Sender);
        void __fastcall X2Y3Click(TObject *Sender);
        void __fastcall X3Y3Click(TObject *Sender);
        void __fastcall X4Y3Click(TObject *Sender);
        void __fastcall X5Y3Click(TObject *Sender);
        void __fastcall X6Y3Click(TObject *Sender);
        void __fastcall X7Y3Click(TObject *Sender);
        void __fastcall X8Y3Click(TObject *Sender);
        void __fastcall X1Y4Click(TObject *Sender);
        void __fastcall X2Y4Click(TObject *Sender);
        void __fastcall X3Y4Click(TObject *Sender);
        void __fastcall X4Y4Click(TObject *Sender);
        void __fastcall X5Y4Click(TObject *Sender);
        void __fastcall X6Y4Click(TObject *Sender);
        void __fastcall X7Y4Click(TObject *Sender);
        void __fastcall X8Y4Click(TObject *Sender);
        void __fastcall X1Y5Click(TObject *Sender);
        void __fastcall X2Y5Click(TObject *Sender);
        void __fastcall X3Y5Click(TObject *Sender);
        void __fastcall X4Y5Click(TObject *Sender);
        void __fastcall X5Y5Click(TObject *Sender);
        void __fastcall X6Y5Click(TObject *Sender);
        void __fastcall X7Y5Click(TObject *Sender);
        void __fastcall X8Y5Click(TObject *Sender);
        void __fastcall X1Y6Click(TObject *Sender);
        void __fastcall X2Y6Click(TObject *Sender);
        void __fastcall X3Y6Click(TObject *Sender);
        void __fastcall X4Y6Click(TObject *Sender);
        void __fastcall X5Y6Click(TObject *Sender);
        void __fastcall X6Y6Click(TObject *Sender);
        void __fastcall X7Y6Click(TObject *Sender);
        void __fastcall X8Y6Click(TObject *Sender);
        void __fastcall X1Y7Click(TObject *Sender);
        void __fastcall X2Y7Click(TObject *Sender);
        void __fastcall X3Y7Click(TObject *Sender);
        void __fastcall X4Y7Click(TObject *Sender);
        void __fastcall X5Y7Click(TObject *Sender);
        void __fastcall X6Y7Click(TObject *Sender);
        void __fastcall X7Y7Click(TObject *Sender);
        void __fastcall X8Y7Click(TObject *Sender);
        void __fastcall X1Y8Click(TObject *Sender);
        void __fastcall X2Y8Click(TObject *Sender);
        void __fastcall X3Y8Click(TObject *Sender);
        void __fastcall X4Y8Click(TObject *Sender);
        void __fastcall X5Y8Click(TObject *Sender);
        void __fastcall X6Y8Click(TObject *Sender);
        void __fastcall X7Y8Click(TObject *Sender);
        void __fastcall X8Y8Click(TObject *Sender);
        void __fastcall ImageMatClick(TObject *Sender);
private:	// User declarations
        Gra gra;
        Ranking ranking;
        std::map<Pole,TImage * *> pola;
        Pole zewnatrz;
        Pole skad;
        Pole dokad;
        void inicjalizuj();
        void wyczyscPola();
        void odswiezPlansze();
        void kliknijPole(int x, int y);
        void przesunFigure(Pole skad, Pole dokad);
        void utworzGracza(std::string imie);
        void pobierzGraczy();
        bool wczytajGre(std::string nazwaPliku);
        bool zapiszGre(std::string nazwaPliku);
        void zakonczGre();
        std::string intToStr(int n);
public:		// User declarations
        /*!
          Konstruktor wygenerowany automatycznie przez C++ Builder 6
        */
        __fastcall TOknoGlowne(TComponent* Owner);
        /*!
          Metoda zlecajaca utworzenie nowej gry dla podanych graczy.
          Metoda publiczna poniewaz jest wywolywana z okna wyboru graczy.

          \param gracz1 Gracz kontrolujacy biale figury
          \param gracz2 Gracz kontrolujacy czarne figury
        */
        void utworzGre(Gracz gracz1, Gracz gracz2);
};
//---------------------------------------------------------------------------
extern PACKAGE TOknoGlowne *OknoGlowne;
//---------------------------------------------------------------------------
#endif
