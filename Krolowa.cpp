//---------------------------------------------------------------------------
#pragma hdrstop
#include "Krolowa.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
Krolowa::Krolowa(Kolor k)
{
  kolor = k;
}
//---------------------------------------------------------------------------
bool Krolowa::walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza)
{
  if ( std::abs(skad.x - dokad.x) == 0 )
  {
    if ( skad.y < dokad.y )
    {
      for ( int y = skad.y + 1; y < dokad.y; y++ )
        if ( plansza.find(Pole(skad.x, y)) != plansza.end() )
          return false;
    }
    else
    {
      for ( int y = skad.y - 1; y > dokad.y; y-- )
        if ( plansza.find(Pole(skad.x, y)) != plansza.end() )
          return false;
    }
    return true;
  }
  else if ( std::abs(skad.y - dokad.y) == 0 )
  {
    if ( skad.x < dokad.x )
    {
      for ( int x = skad.x + 1; x < dokad.x; x++ )
        if ( plansza.find(Pole(x, skad.y)) != plansza.end() )
          return false;
    }
    else
    {
      for ( int x = skad.x - 1; x > dokad.x; x-- )
        if ( plansza.find(Pole(x, skad.y)) != plansza.end() )
          return false;
    }
    return true;
  }
  else if ( std::abs(skad.x - dokad.x) == std::abs(skad.y - dokad.y) )
  {
    if ( (skad.y < dokad.y) && (skad.x < dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x + i, skad.y + i)) != plansza.end() )
          return false;
    }
    else if ( (skad.y > dokad.y) && (skad.x < dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x + i, skad.y - i)) != plansza.end() )
          return false;
    }
    else if ( (skad.y > dokad.y) && (skad.x > dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x - i, skad.y - i)) != plansza.end() )
          return false;
    }
    else if ( (skad.y < dokad.y) && (skad.x > dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x - i, skad.y + i)) != plansza.end() )
          return false;
    }
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------
std::string Krolowa::przedstawSie()
{
  return "Krolowa";
}
//---------------------------------------------------------------------------
