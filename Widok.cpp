//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "Widok.h"
#include "Autor.h"
#include "NowaGra.h"
#include <typeinfo>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOknoGlowne *OknoGlowne;
//---------------------------------------------------------------------------
__fastcall TOknoGlowne::TOknoGlowne(TComponent* Owner)
        : TForm(Owner)
{
  inicjalizuj();
}
//---------------------------------------------------------------------------
void TOknoGlowne::zakonczGre()
{
  ranking.zapiszPlikRankingu();
  Application -> Terminate();
}
//---------------------------------------------------------------------------
void TOknoGlowne::inicjalizuj()
{
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,1), &X1Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,2), &X1Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,3), &X1Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,4), &X1Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,5), &X1Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,6), &X1Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,7), &X1Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(1,8), &X1Y8 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,1), &X2Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,2), &X2Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,3), &X2Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,4), &X2Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,5), &X2Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,6), &X2Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,7), &X2Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(2,8), &X2Y8 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,1), &X3Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,2), &X3Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,3), &X3Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,4), &X3Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,5), &X3Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,6), &X3Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,7), &X3Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(3,8), &X3Y8 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,1), &X4Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,2), &X4Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,3), &X4Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,4), &X4Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,5), &X4Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,6), &X4Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,7), &X4Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(4,8), &X4Y8 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,1), &X5Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,2), &X5Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,3), &X5Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,4), &X5Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,5), &X5Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,6), &X5Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,7), &X5Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(5,8), &X5Y8 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,1), &X6Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,2), &X6Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,3), &X6Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,4), &X6Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,5), &X6Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,6), &X6Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,7), &X6Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(6,8), &X6Y8 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,1), &X7Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,2), &X7Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,3), &X7Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,4), &X7Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,5), &X7Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,6), &X7Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,7), &X7Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(7,8), &X7Y8 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,1), &X8Y1 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,2), &X8Y2 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,3), &X8Y3 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,4), &X8Y4 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,5), &X8Y5 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,6), &X8Y6 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,7), &X8Y7 ));
  pola.insert(std::pair<Pole,TImage * *>( Pole(8,8), &X8Y8 ));
  ranking.wczytajPlikRankingu();
  pobierzGraczy();
  odswiezPlansze();
}
//---------------------------------------------------------------------------
void TOknoGlowne::wyczyscPola()
{
  X1Y1 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X1Y3 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X1Y5 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X1Y7 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X2Y2 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X2Y4 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X2Y6 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X2Y8 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X3Y1 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X3Y3 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X3Y5 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X3Y7 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X4Y2 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X4Y4 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X4Y6 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X4Y8 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X5Y1 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X5Y3 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X5Y5 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X5Y7 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X6Y2 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X6Y4 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X6Y6 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X6Y8 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X7Y1 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X7Y3 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X7Y5 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X7Y7 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X8Y2 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X8Y4 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X8Y6 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X8Y8 -> Picture -> LoadFromFile("img/CPuste.jpg");
  X1Y2 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X1Y4 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X1Y6 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X1Y8 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X2Y1 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X2Y3 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X2Y5 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X2Y7 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X3Y2 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X3Y4 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X3Y6 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X3Y8 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X4Y1 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X4Y3 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X4Y5 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X4Y7 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X5Y2 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X5Y4 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X5Y6 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X5Y8 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X6Y1 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X6Y3 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X6Y5 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X6Y7 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X7Y2 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X7Y4 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X7Y6 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X7Y8 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X8Y1 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X8Y3 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X8Y5 -> Picture -> LoadFromFile("img/BPuste.jpg");
  X8Y7 -> Picture -> LoadFromFile("img/BPuste.jpg");
}

//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::NowyGraczButtonClick(TObject *Sender)
{
  utworzGracza((NowyGraczInput -> Text).c_str());
  pobierzGraczy();
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::Nowagra1Click(TObject *Sender)
{
  OknoNowaGra -> wczytajGraczy(ranking.pobierzGraczy());
  OknoNowaGra -> ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::Wczytajgre1Click(TObject *Sender)
{
  if (WczytajGreDialog -> Execute())
  {
    try
    {
      if (wczytajGre((WczytajGreDialog -> FileName).c_str()));
        ShowMessage("Gra zostala wczytana");
    }
    catch (...)
    {
      ShowMessage("Blad podczas otwierania pliku");
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::Zapiszgre1Click(TObject *Sender)
{
  if (ZapiszGreDialog -> Execute())
  {
    try
    {
      if (zapiszGre((ZapiszGreDialog -> FileName).c_str()))
        ShowMessage("Gra zostala zapisana");
    }
    catch (...)
    {
      ShowMessage("Blad podczas zapisywania pliku");
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::Zakoncz1Click(TObject *Sender)
{
  zakonczGre();
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::FormClose(TObject *Sender, TCloseAction &Action)
{
  zakonczGre();        
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::Autor1Click(TObject *Sender)
{
  OknoAutor -> ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::Zasadygry1Click(TObject *Sender)
{
  ShellExecute(NULL, "open", "https://pl.wikipedia.org/wiki/Zasady_gry_w_szachy",
    NULL, NULL, SW_SHOWNORMAL);        
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::ImageMatClick(TObject *Sender)
{
  OknoNowaGra -> wczytajGraczy(ranking.pobierzGraczy());
  OknoNowaGra -> ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TOknoGlowne::X1Y1Click(TObject *Sender)
{
  kliknijPole(1,1);
}
void __fastcall TOknoGlowne::X2Y1Click(TObject *Sender)
{
  kliknijPole(2,1);
}
void __fastcall TOknoGlowne::X3Y1Click(TObject *Sender)
{
  kliknijPole(3,1);
}
void __fastcall TOknoGlowne::X4Y1Click(TObject *Sender)
{
  kliknijPole(4,1);
}
void __fastcall TOknoGlowne::X5Y1Click(TObject *Sender)
{
  kliknijPole(5,1);
}
void __fastcall TOknoGlowne::X6Y1Click(TObject *Sender)
{
  kliknijPole(6,1);
}
void __fastcall TOknoGlowne::X7Y1Click(TObject *Sender)
{
  kliknijPole(7,1);
}
void __fastcall TOknoGlowne::X8Y1Click(TObject *Sender)
{
  kliknijPole(8,1);
}
void __fastcall TOknoGlowne::X1Y2Click(TObject *Sender)
{
  kliknijPole(1,2);
}
void __fastcall TOknoGlowne::X2Y2Click(TObject *Sender)
{
  kliknijPole(2,2);
}
void __fastcall TOknoGlowne::X3Y2Click(TObject *Sender)
{
  kliknijPole(3,2);
}
void __fastcall TOknoGlowne::X4Y2Click(TObject *Sender)
{
  kliknijPole(4,2);
}
void __fastcall TOknoGlowne::X5Y2Click(TObject *Sender)
{
  kliknijPole(5,2);
}
void __fastcall TOknoGlowne::X6Y2Click(TObject *Sender)
{
  kliknijPole(6,2);
}
void __fastcall TOknoGlowne::X7Y2Click(TObject *Sender)
{
  kliknijPole(7,2);
}
void __fastcall TOknoGlowne::X8Y2Click(TObject *Sender)
{
  kliknijPole(8,2);
}
void __fastcall TOknoGlowne::X1Y3Click(TObject *Sender)
{
  kliknijPole(1,3);
}
void __fastcall TOknoGlowne::X2Y3Click(TObject *Sender)
{
  kliknijPole(2,3);
}
void __fastcall TOknoGlowne::X3Y3Click(TObject *Sender)
{
  kliknijPole(3,3);
}
void __fastcall TOknoGlowne::X4Y3Click(TObject *Sender)
{
  kliknijPole(4,3);
}
void __fastcall TOknoGlowne::X5Y3Click(TObject *Sender)
{
  kliknijPole(5,3);
}
void __fastcall TOknoGlowne::X6Y3Click(TObject *Sender)
{
  kliknijPole(6,3);
}
void __fastcall TOknoGlowne::X7Y3Click(TObject *Sender)
{
  kliknijPole(7,3);
}
void __fastcall TOknoGlowne::X8Y3Click(TObject *Sender)
{
  kliknijPole(8,3);
}
void __fastcall TOknoGlowne::X1Y4Click(TObject *Sender)
{
  kliknijPole(1,4);
}
void __fastcall TOknoGlowne::X2Y4Click(TObject *Sender)
{
  kliknijPole(2,4);
}
void __fastcall TOknoGlowne::X3Y4Click(TObject *Sender)
{
  kliknijPole(3,4);
}
void __fastcall TOknoGlowne::X4Y4Click(TObject *Sender)
{
  kliknijPole(4,4);
}
void __fastcall TOknoGlowne::X5Y4Click(TObject *Sender)
{
  kliknijPole(5,4);
}
void __fastcall TOknoGlowne::X6Y4Click(TObject *Sender)
{
  kliknijPole(6,4);
}
void __fastcall TOknoGlowne::X7Y4Click(TObject *Sender)
{
  kliknijPole(7,4);
}
void __fastcall TOknoGlowne::X8Y4Click(TObject *Sender)
{
  kliknijPole(8,4);
}
void __fastcall TOknoGlowne::X1Y5Click(TObject *Sender)
{
  kliknijPole(1,5);
}
void __fastcall TOknoGlowne::X2Y5Click(TObject *Sender)
{
  kliknijPole(2,5);
}
void __fastcall TOknoGlowne::X3Y5Click(TObject *Sender)
{
  kliknijPole(3,5);
}
void __fastcall TOknoGlowne::X4Y5Click(TObject *Sender)
{
  kliknijPole(4,5);
}
void __fastcall TOknoGlowne::X5Y5Click(TObject *Sender)
{
  kliknijPole(5,5);
}
void __fastcall TOknoGlowne::X6Y5Click(TObject *Sender)
{
  kliknijPole(6,5);
}
void __fastcall TOknoGlowne::X7Y5Click(TObject *Sender)
{
  kliknijPole(7,5);
}
void __fastcall TOknoGlowne::X8Y5Click(TObject *Sender)
{
  kliknijPole(8,5);
}
void __fastcall TOknoGlowne::X1Y6Click(TObject *Sender)
{
  kliknijPole(1,6);
}
void __fastcall TOknoGlowne::X2Y6Click(TObject *Sender)
{
  kliknijPole(2,6);
}
void __fastcall TOknoGlowne::X3Y6Click(TObject *Sender)
{
  kliknijPole(3,6);
}
void __fastcall TOknoGlowne::X4Y6Click(TObject *Sender)
{
  kliknijPole(4,6);
}
void __fastcall TOknoGlowne::X5Y6Click(TObject *Sender)
{
  kliknijPole(5,6);
}
void __fastcall TOknoGlowne::X6Y6Click(TObject *Sender)
{
  kliknijPole(6,6);
}
void __fastcall TOknoGlowne::X7Y6Click(TObject *Sender)
{
  kliknijPole(7,6);
}
void __fastcall TOknoGlowne::X8Y6Click(TObject *Sender)
{
  kliknijPole(8,6);
}
void __fastcall TOknoGlowne::X1Y7Click(TObject *Sender)
{
  kliknijPole(1,7);
}
void __fastcall TOknoGlowne::X2Y7Click(TObject *Sender)
{
  kliknijPole(2,7);
}
void __fastcall TOknoGlowne::X3Y7Click(TObject *Sender)
{
  kliknijPole(3,7);
}
void __fastcall TOknoGlowne::X4Y7Click(TObject *Sender)
{
  kliknijPole(4,7);
}
void __fastcall TOknoGlowne::X5Y7Click(TObject *Sender)
{
  kliknijPole(5,7);
}
void __fastcall TOknoGlowne::X6Y7Click(TObject *Sender)
{
  kliknijPole(6,7);
}
void __fastcall TOknoGlowne::X7Y7Click(TObject *Sender)
{
  kliknijPole(7,7);
}
void __fastcall TOknoGlowne::X8Y7Click(TObject *Sender)
{
  kliknijPole(8,7);
}
void __fastcall TOknoGlowne::X1Y8Click(TObject *Sender)
{
  kliknijPole(1,8);
}
void __fastcall TOknoGlowne::X2Y8Click(TObject *Sender)
{
  kliknijPole(2,8);
}
void __fastcall TOknoGlowne::X3Y8Click(TObject *Sender)
{
  kliknijPole(3,8);
}
void __fastcall TOknoGlowne::X4Y8Click(TObject *Sender)
{
  kliknijPole(4,8);
}
void __fastcall TOknoGlowne::X5Y8Click(TObject *Sender)
{
  kliknijPole(5,8);
}
void __fastcall TOknoGlowne::X6Y8Click(TObject *Sender)
{
  kliknijPole(6,8);
}
void __fastcall TOknoGlowne::X7Y8Click(TObject *Sender)
{
  kliknijPole(7,8);
}
void __fastcall TOknoGlowne::X8Y8Click(TObject *Sender)
{
  kliknijPole(8,8);
}
//---------------------------------------------------------------------------
std::string TOknoGlowne::intToStr(int n)
{
     std::string tmp;
     if(n < 0) {
      tmp = "-";
      n = -n;
     }
     if(n > 9)
      tmp += intToStr(n / 10);
     tmp += n % 10 + 48;
     return tmp;
}
//---------------------------------------------------------------------------
void TOknoGlowne::pobierzGraczy()
{
  std::list<Gracz> gracze = ranking.pobierzGraczy();
  std::list<Gracz>::iterator it;
  RankingListBox -> Items -> Clear();
  for (it = gracze.begin(); it != gracze.end(); ++it)
  {
    RankingListBox -> Items -> Add((
      it -> pobierzImie() + " ["
      + intToStr(it -> pobierzWygrane()) + "/"
      + intToStr(it -> pobierzRemisy()) + "/"
      + intToStr(it -> pobierzPorazki()) + "]"
    ).c_str());
  }
}
//---------------------------------------------------------------------------
void TOknoGlowne::utworzGracza(std::string imie)
{
  ranking.utworzGracza(imie);
}
//---------------------------------------------------------------------------
void TOknoGlowne::utworzGre(Gracz gracz1, Gracz gracz2)
{
  gra.utworzGre(gracz1, gracz2);
  ImageMat -> Visible = false;
  odswiezPlansze();
}
//---------------------------------------------------------------------------
bool TOknoGlowne::wczytajGre(std::string nazwaPliku)
{
  bool wczytano = gra.wczytajGre(nazwaPliku);
  ImageMat -> Visible = false;
  odswiezPlansze();
  return wczytano;
}
//---------------------------------------------------------------------------
bool TOknoGlowne::zapiszGre(std::string nazwaPliku)
{
  return gra.zapiszGre(nazwaPliku);
}
//---------------------------------------------------------------------------
void TOknoGlowne::kliknijPole(int x, int y)
{
  if (skad.x == x && skad.y == y)
  {
    skad = zewnatrz;
  }
  else if (skad.x == 0)
  {
    skad = Pole(x,y);
  }
  else
  {
    dokad = Pole(x,y);
    przesunFigure(skad, dokad);
  }
}
//---------------------------------------------------------------------------
void TOknoGlowne::przesunFigure(Pole skad, Pole dokad)
{
  if (!gra.przesunFigure(skad, dokad))
    ShowMessage("Niedozwolony ruch!");
  odswiezPlansze();
  if (gra.sprawdzCzySzach(gra.sprawdzKtoRusza()))
  {
    if (gra.sprawdzCzyMat(gra.sprawdzKtoRusza()))
    {
      ShowMessage("MAT! KONIEC GRY!");
      ImageMat -> Visible = true;
      if ( gra.sprawdzKtoRusza() == BIALY )
      {
        ranking.odnotujZwyciestwo(gra.pobierzGraczy().find(CZARNY) -> second.pobierzImie());
        ranking.odnotujPorazke(gra.pobierzGraczy().find(BIALY) -> second.pobierzImie());
      }
      else
      {
        ranking.odnotujZwyciestwo(gra.pobierzGraczy().find(BIALY) -> second.pobierzImie());
        ranking.odnotujPorazke(gra.pobierzGraczy().find(CZARNY) -> second.pobierzImie());
      }
      pobierzGraczy();
    }
    else ShowMessage("Szach!");
  }
}
//---------------------------------------------------------------------------
void TOknoGlowne::odswiezPlansze()
{
  std::map<Kolor,Gracz> gracze = gra.pobierzGraczy();
  GraczBialyLabel -> Caption = gracze.find(BIALY) -> second.pobierzImie().c_str();
  GraczCzarnyLabel -> Caption = gracze.find(CZARNY) -> second.pobierzImie().c_str();
  if (gra.sprawdzKtoRusza() == BIALY)
    KogoRuchShape -> Brush -> Color = clWhite;
  else
    KogoRuchShape -> Brush -> Color = clBlack;
  wyczyscPola();
  std::map<Pole,Figura*> plansza = gra.pobierzPlansze();
  std::map<Pole,Figura*>::iterator it;
  for (it = plansza.begin(); it != plansza.end(); it++)
  {
    TImage *tim = *pola[Pole(it->first.x, it->first.y)];
    AnsiString kolorPola = "B";
    if (it->first.kolor == CZARNY)
      kolorPola = "C";
    AnsiString figura = it -> second -> przedstawSie().c_str();
    AnsiString kolorFigury = "B";
    if (it->second->sprawdzKolor() == CZARNY)
      kolorFigury = "C";
    tim -> Picture -> LoadFromFile("img/" + kolorPola + figura + kolorFigury + ".jpg");
  }
  skad = zewnatrz;
  dokad = zewnatrz;
}
//---------------------------------------------------------------------------
