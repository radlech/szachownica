//---------------------------------------------------------------------------
#pragma hdrstop
#include "Wieza.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
Wieza::Wieza(Kolor k)
{
  kolor = k;
}
//---------------------------------------------------------------------------
bool Wieza::walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza)
{
  if ( std::abs(skad.x - dokad.x) == 0 )
  {
    if ( skad.y < dokad.y )
    {
      for ( int y = skad.y + 1; y < dokad.y; y++ )
        if ( plansza.find(Pole(skad.x, y)) != plansza.end() )
          return false;
    }
    else
    {
      for ( int y = skad.y - 1; y > dokad.y; y-- )
        if ( plansza.find(Pole(skad.x, y)) != plansza.end() )
          return false;
    }
    return true;
  }
  else if ( std::abs(skad.y - dokad.y) == 0 )
  {
    if ( skad.x < dokad.x )
    {
      for ( int x = skad.x + 1; x < dokad.x; x++ )
        if ( plansza.find(Pole(x, skad.y)) != plansza.end() )
          return false;
    }
    else
    {
      for ( int x = skad.x - 1; x > dokad.x; x-- )
        if ( plansza.find(Pole(x, skad.y)) != plansza.end() )
          return false;
    }
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------
std::string Wieza::przedstawSie()
{
  return "Wieza";
}
//---------------------------------------------------------------------------
