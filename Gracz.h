//---------------------------------------------------------------------------
#ifndef GraczH
#define GraczH
#include <string>
//---------------------------------------------------------------------------
/*!
  Klasa odpowiedzialna za przechowywanie statystyk gracza.
*/
class Gracz
{
private:
  std::string imie;
  int wygrane;
  int remisy;
  int porazki;
public:

  /*!
    Konstruktor tworzacy gracza z zerowymi statystykami

    \param i Imie gracza
  */
  Gracz(std::string i);

  /*!
    Konstruktor tworzacy gracza z podanymi statystykami

    \param i Imie gracza
    \param w Liczba wygranych partii
    \param r Liczba zremisowanych partii
    \param p Liczba przegranych partii
  */
  Gracz(std::string i, int w, int r, int p);

  /*!
    Metoda dodajaca jedno zwyciestwo do statystyk
  */
  void odnotujZwyciestwo();

  /*!
    Metoda dodajaca jeden remis do statystyk
  */
  void odnotujRemis();

  /*!
    Metoda dodajaca jedna porazke do statystyk
  */
  void odnotujPorazke();

  /*!
    Metoda pobierajaca imie gracza

    \return Imie gracza
  */
  std::string pobierzImie();

  /*!
    Metoda pobierajaca liczbe wygranych gracza

    \return Liczba wygranych
  */
  int pobierzWygrane();

  /*!
    Metoda pobierajaca liczbe remisow gracza

    \return Liczba remisow
  */
  int pobierzRemisy();

  /*!
    Metoda pobierajaca liczba porazek gracza

    \return Liczba porazek
  */
  int pobierzPorazki();
};
//---------------------------------------------------------------------------
#endif
