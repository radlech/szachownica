//---------------------------------------------------------------------------
#pragma hdrstop
#include "Ranking.h"
#include <iostream>
#include <fstream>
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
void Ranking::utworzGracza(std::string imie)
{
  gracze.push_back(Gracz(imie));
  zapiszPlikRankingu();
}
//---------------------------------------------------------------------------
std::list<Gracz> Ranking::pobierzGraczy()
{
  return gracze;
}
//---------------------------------------------------------------------------
void Ranking::odnotujZwyciestwo(std::string imieGracza)
{
  std::list<Gracz>::iterator it = gracze.begin();
  while (it != gracze.end())
  {
    if ( it->pobierzImie() == imieGracza )
    {
      it->odnotujZwyciestwo();
      return;
    }
    it++;
  }
  gracze.push_back(Gracz(imieGracza,1,0,0));
}
//---------------------------------------------------------------------------
void Ranking::odnotujRemis(std::string imieGracza)
{
  std::list<Gracz>::iterator it = gracze.begin();
  while (it != gracze.end())
  {
    if ( it->pobierzImie() == imieGracza )
    {
      it->odnotujRemis();
      return;
    }
    it++;
  }
  gracze.push_back(Gracz(imieGracza,0,1,0));
}
//---------------------------------------------------------------------------
void Ranking::odnotujPorazke(std::string imieGracza)
{
  std::list<Gracz>::iterator it = gracze.begin();
  while (it != gracze.end())
  {
    if ( it->pobierzImie() == imieGracza )
    {
      it->odnotujPorazke();
      return;
    }
    it++;
  }
  gracze.push_back(Gracz(imieGracza,0,0,1));
}
//---------------------------------------------------------------------------
void Ranking::wczytajPlikRankingu()
{
  std::fstream plik;
  std::string dane;
  plik.open("players/players.rnk", std::ios::in);
  if ( plik.good() == true )
  {
    int i = 0;
    std::string imie;
    int w, r, p;
    while ( getline(plik,dane) )
    {
      switch(i)
      {
      case 0:
        imie = dane;
        i++;
        break;
      case 1:
        w = std::atoi(dane.c_str());
        i++;
        break;
      case 2:
        r = std::atoi(dane.c_str());
        i++;
        break;
      case 3:
        p = std::atoi(dane.c_str());
        gracze.push_back(Gracz(imie, w, r, p));
        i = 0;
        break;
      }
    }
    plik.close();
  }
}
//---------------------------------------------------------------------------
void Ranking::zapiszPlikRankingu()
{
  std::fstream plik;
  std::string dane;
  plik.open("players/players.rnk", std::ios::out | std::ios::trunc);
  if ( plik.good() == true )
  {
    std::list<Gracz>::iterator it = gracze.begin();
    while (it != gracze.end())
    {
      plik << it->pobierzImie() << std::endl;
      plik << it->pobierzWygrane() << std::endl;
      plik << it->pobierzRemisy() << std::endl;
      plik << it->pobierzPorazki() << std::endl;
      it++;
    }
    plik.close();
  }
}
//---------------------------------------------------------------------------
