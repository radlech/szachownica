//---------------------------------------------------------------------------
#ifndef RankingH
#define RankingH
#include "Gracz.h"
#include <string>
#include <list>
//---------------------------------------------------------------------------
/*!
  Klasa odpowiedzialna za liste rankingowa graczy.
*/
class Ranking
{
private:
  std::list<Gracz> gracze;
public:

  /*!
    Metoda dodajaca do listy rankingowej nowego gracza o podanym imieniu.

    \param imie Imie gracza
  */
  void utworzGracza(std::string imie);

  /*!
    Metoda pobierajaca liste rankingowa

    \return Lista graczy
  */
  std::list<Gracz> pobierzGraczy();

  /*!
    Metoda odnotowujaca zwyciestwo podanego gracza

    \param imieGracza Imie gracza
  */
  void odnotujZwyciestwo(std::string imieGracza);

  /*!
    Metoda odnotowujaca remis podanego gracza

    \param imieGracza Imie gracza
  */
  void odnotujRemis(std::string imieGracza);

  /*!
    Metoda odnotowujaca porazke podanego gracza

    \param imieGracza Imie gracza
  */
  void odnotujPorazke(std::string imieGracza);

  /*!
    Metoda wczytujaca liste rankingowa z pliku
  */
  void wczytajPlikRankingu();

  /*!
    Metoda zapisujaca liste rankingowa do pliku
  */
  void zapiszPlikRankingu();
};
//---------------------------------------------------------------------------
#endif
