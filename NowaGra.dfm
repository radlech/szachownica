object OknoNowaGra: TOknoNowaGra
  Left = 324
  Top = 495
  BorderStyle = bsToolWindow
  Caption = 'Nowa gra'
  ClientHeight = 128
  ClientWidth = 248
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object LabelWybierzGraczy: TLabel
    Left = 8
    Top = 8
    Width = 75
    Height = 13
    Caption = 'Wybierz graczy:'
  end
  object ShapeBialy: TShape
    Left = 8
    Top = 32
    Width = 17
    Height = 17
  end
  object ShapeCzarny: TShape
    Left = 8
    Top = 64
    Width = 17
    Height = 17
    Brush.Color = clBlack
  end
  object ComboBoxBialy: TComboBox
    Left = 32
    Top = 32
    Width = 209
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    MaxLength = 10
    TabOrder = 0
  end
  object ComboBoxCzarny: TComboBox
    Left = 32
    Top = 64
    Width = 209
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    MaxLength = 10
    TabOrder = 1
  end
  object ButtonNowaGra: TButton
    Left = 8
    Top = 96
    Width = 233
    Height = 25
    Caption = 'Rozpocznij gre'
    TabOrder = 2
    OnClick = ButtonNowaGraClick
  end
end
