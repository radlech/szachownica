//---------------------------------------------------------------------------
#pragma hdrstop
#include "Krol.h"
#include <cstdlib.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
Krol::Krol(Kolor k)
{
  kolor = k;
}
//---------------------------------------------------------------------------
bool Krol::walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza)
{
  if ( std::abs(skad.x - dokad.x) <= 1 && std::abs(skad.y - dokad.y) <= 1 )
    return true;
  return false;
}
//---------------------------------------------------------------------------
std::string Krol::przedstawSie()
{
  return "Krol";
}
//---------------------------------------------------------------------------
