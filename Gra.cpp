//---------------------------------------------------------------------------
#pragma hdrstop
#include "Gra.h"
#include <fstream>
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
template <typename T>
std::pair<Pole,Figura*> przygotujFigure(T *figura, int x, int y)
{
  Pole pole = Pole(x, y);
  return std::pair<Pole,Figura*>(pole, figura);
}
//---------------------------------------------------------------------------
void Gra::utworzGre(Gracz gracz1, Gracz gracz2)
{
  kogoRuch = BIALY;
  gracze.clear();
  gracze.insert( std::pair<Kolor,Gracz>(BIALY, gracz1) );
  gracze.insert( std::pair<Kolor,Gracz>(CZARNY, gracz2) );
  plansza.clear();
  plansza.insert( przygotujFigure(new Wieza(BIALY), 1, 1) );
  plansza.insert( przygotujFigure(new Skoczek(BIALY), 2, 1) );
  plansza.insert( przygotujFigure(new Goniec(BIALY), 3, 1) );
  plansza.insert( przygotujFigure(new Krolowa(BIALY), 4, 1) );
  plansza.insert( przygotujFigure(new Krol(BIALY), 5, 1) );
  plansza.insert( przygotujFigure(new Goniec(BIALY), 6, 1) );
  plansza.insert( przygotujFigure(new Skoczek(BIALY), 7, 1) );
  plansza.insert( przygotujFigure(new Wieza(BIALY), 8, 1) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 1, 2) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 2, 2) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 3, 2) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 4, 2) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 5, 2) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 6, 2) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 7, 2) );
  plansza.insert( przygotujFigure(new Pion(BIALY), 8, 2) );
  plansza.insert( przygotujFigure(new Wieza(CZARNY), 1, 8) );
  plansza.insert( przygotujFigure(new Skoczek(CZARNY), 2, 8) );
  plansza.insert( przygotujFigure(new Goniec(CZARNY), 3, 8) );
  plansza.insert( przygotujFigure(new Krolowa(CZARNY), 4, 8) );
  plansza.insert( przygotujFigure(new Krol(CZARNY), 5, 8) );
  plansza.insert( przygotujFigure(new Goniec(CZARNY), 6, 8) );
  plansza.insert( przygotujFigure(new Skoczek(CZARNY), 7, 8) );
  plansza.insert( przygotujFigure(new Wieza(CZARNY), 8, 8) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 1, 7) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 2, 7) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 3, 7) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 4, 7) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 5, 7) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 6, 7) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 7, 7) );
  plansza.insert( przygotujFigure(new Pion(CZARNY), 8, 7) );
}
//---------------------------------------------------------------------------
bool Gra::wczytajGre(std::string nazwaPliku)
{
  std::fstream plik;
  std::string dane;
  plik.open(nazwaPliku.c_str(), std::ios::in);
  if ( plik.good() == true )
  {
    getline(plik, dane);
    if (dane == "B")
      kogoRuch = BIALY;
    else
      kogoRuch = CZARNY;
    gracze.clear();
    getline(plik, dane);
    gracze.insert( std::pair<Kolor,Gracz>(BIALY, Gracz(dane)) );
    getline(plik, dane);
    gracze.insert( std::pair<Kolor,Gracz>(CZARNY, Gracz(dane)) );
    plansza.clear();
    while ( getline(plik,dane) )
    {
      int x = dane.at(2) - '0';
      int y = dane.at(3) - '0';
      Kolor k = BIALY;
      if (dane.at(0) == 'C')
        k = CZARNY;
      switch (dane.at(1))
      {
      case 'P':
        plansza.insert( przygotujFigure(new Pion(k), x, y) );
        break;
      case 'K':
        plansza.insert( przygotujFigure(new Krol(k), x, y) );
        break;
      case 'Q':
        plansza.insert( przygotujFigure(new Krolowa(k), x, y) );
        break;
      case 'G':
        plansza.insert( przygotujFigure(new Goniec(k), x, y) );
        break;
      case 'S':
        plansza.insert( przygotujFigure(new Skoczek(k), x, y) );
        break;
      case 'W':
        plansza.insert( przygotujFigure(new Wieza(k), x, y) );
        break;
      }
    }
    plik.close();
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------
bool Gra::zapiszGre(std::string nazwaPliku)
{
  std::fstream plik;
  std::string dane;
  plik.open(nazwaPliku.c_str(), std::ios::out | std::ios::trunc);
  if ( plik.good() == true )
  {
    plik << ( (kogoRuch == BIALY) ? "B" : "C" ) << std::endl;
    plik << gracze.find(BIALY) -> second.pobierzImie() << std::endl;
    plik << gracze.find(CZARNY) -> second.pobierzImie();
    std::map<Pole,Figura*>::iterator it = plansza.begin();
    while (it != plansza.end())
    {
      plik << std::endl;
      plik << ( (it->second->sprawdzKolor() == BIALY) ? "B" : "C" );
      std::string figura = it->second->przedstawSie();
      if (figura == "Pion")
        plik << "P";
      else if (figura == "Krol")
        plik << "K";
      else if (figura == "Krolowa")
        plik << "Q";
      else if (figura == "Goniec")
        plik << "G";
      else if (figura == "Skoczek")
        plik << "S";
      else if (figura == "Wieza")
        plik << "W";
      plik << it->first.x;
      plik << it->first.y;
      it++;
    }
    plik.close();
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------
std::map<Pole,Figura*> Gra::pobierzPlansze()
{
  return plansza;
}
//---------------------------------------------------------------------------
std::map<Kolor,Gracz> Gra::pobierzGraczy()
{
  return gracze;
}
//---------------------------------------------------------------------------
Kolor Gra::sprawdzKtoRusza()
{
  return kogoRuch;
}
//---------------------------------------------------------------------------
bool Gra::przesunFigure(Pole skad, Pole dokad, bool trybTestowy)
{
  std::map<Pole,Figura*> *p;
  if ( trybTestowy )
    p = new std::map<Pole,Figura*>(plansza);
  else
    p = &plansza;
  std::map<Pole,Figura*> planszaPrzedRuchem = *p;
  if
  (
    // sprawdzenie czy na polu SKAD jest figura wlasciwego koloru
    ( p->find(skad) != p->end() && p->find(skad)->second->sprawdzKolor() == kogoRuch )
    // sprawdzenie czy na polu DOKAD nie ma figury tego samego koloru
    && ( p->find(dokad) == p->end() || p->find(dokad)->second->sprawdzKolor() != p->find(skad)->second->sprawdzKolor() )
    // sprawdzenie czy ruch jest dozwolony dla danej figury
    && ( p->find(skad)->second->walidujRuch(skad, dokad, *p) == true )
  )
  {
    p->erase( dokad );
    p->insert( std::pair<Pole,Figura*>(dokad, p->find(skad)->second) );
    p->erase( skad );
    // sprawdzenie czy po wykonaniu ruchu nie ma szacha
    if ( sprawdzCzySzach(kogoRuch, p) )
    {
      *p = planszaPrzedRuchem;
      return false;
    }
    if ( !trybTestowy )
        kogoRuch = !kogoRuch;
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------
bool Gra::sprawdzCzySzach(Kolor kolor, std::map<Pole,Figura*> *planszaTestowa)
{
  std::map<Pole,Figura*> *p;
  if ( planszaTestowa != 0 )
    p = planszaTestowa;
  else
    p = &plansza;
  Pole krol;
  std::map<Pole,Figura*>::iterator it = p->begin();
  while ( it != p->end() )
  {
    if ( it->second->sprawdzKolor() == kolor && it->second->przedstawSie() == "Krol" )
      krol = it->first;
    it++;
  }
  it = p->begin();
  while ( it != p->end() )
  {
    if ( it->second->sprawdzKolor() != kolor && it->second->walidujRuch(it->first, krol, *p) == true )
      return true;
    it++;
  }
  return false;
}
//---------------------------------------------------------------------------
bool Gra::sprawdzCzyMat(Kolor kolor)
{
  std::map<Pole,Figura*>::iterator it = plansza.begin();
  while ( it != plansza.end() )
  {
    if ( it->second->sprawdzKolor() == kolor )
    {
      for( int x = 1; x <= 8; x++ )
      {
        for ( int y = 1; y <= 8; y++ )
        {
          if ( przesunFigure(it->first, Pole(x,y), true) )
          {
            return false;
          }
        }
      }
    }
    it++;
  }
  return true;
}
//---------------------------------------------------------------------------
