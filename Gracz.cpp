//---------------------------------------------------------------------------
#pragma hdrstop
#include "Gracz.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
Gracz::Gracz(std::string i)
{
  imie = i;
  wygrane = 0;
  remisy = 0;
  porazki = 0;
}
//---------------------------------------------------------------------------
Gracz::Gracz(std::string i, int w, int r, int p)
{
  imie = i;
  wygrane = w;
  remisy = r;
  porazki = p;
}
//---------------------------------------------------------------------------
void Gracz::odnotujZwyciestwo()
{
  wygrane++;
}
//---------------------------------------------------------------------------
void Gracz::odnotujRemis()
{
  remisy++;
}
//---------------------------------------------------------------------------
void Gracz::odnotujPorazke()
{
  porazki++;
}
//---------------------------------------------------------------------------
std::string Gracz::pobierzImie()
{
  return imie;
}
//---------------------------------------------------------------------------
int Gracz::pobierzWygrane()
{
  return wygrane;
}
//---------------------------------------------------------------------------
int Gracz::pobierzRemisy()
{
  return remisy;
}
//---------------------------------------------------------------------------
int Gracz::pobierzPorazki()
{
  return porazki;
}
//---------------------------------------------------------------------------
