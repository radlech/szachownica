//---------------------------------------------------------------------------
#ifndef PoleH
#define PoleH
#include <Kolor.h>
//---------------------------------------------------------------------------
/*!
  Struktura odpowiedzialna za przechowywanie informacji o polu szachownicy.
*/
struct Pole {
  int x;
  int y;
  Kolor kolor;

  Pole();
  Pole(int xval, int yval);

  bool operator<(const Pole &o)  const
  {
    return x < o.x || (x == o.x && y < o.y);
  }

  bool operator==(const Pole &o) const
  {
    return x == o.x && y == o.y;
  }
};
//---------------------------------------------------------------------------
#endif
