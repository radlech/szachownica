//---------------------------------------------------------------------------
#ifndef KolorH
#define KolorH
//---------------------------------------------------------------------------
/*!
  Typ wyliczeniowy odpowiedzialny za przechowywanie koloru figur, graczy
  oraz pol na szachownicy.
*/
enum Kolor {
  BIALY,
  CZARNY
};
//---------------------------------------------------------------------------
#endif
