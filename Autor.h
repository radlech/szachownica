//---------------------------------------------------------------------------
#ifndef AutorH
#define AutorH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
/*!
  Klasa widoku odpowiedzialna za okno wyswietlajace informacje o autorze.
*/
class TOknoAutor : public TForm
{
__published:	// IDE-managed Components
        TImage *Image1;
        TButton *Button1;
        TStaticText *StaticText1;
        TStaticText *StaticText2;
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        /*!
          Konstruktor utworzony automatycznie przez C++ Builder 6
        */
        __fastcall TOknoAutor(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TOknoAutor *OknoAutor;
//---------------------------------------------------------------------------
#endif
