//---------------------------------------------------------------------------
#ifndef NowaGraH
#define NowaGraH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <gracz.h>
#include <list>
//---------------------------------------------------------------------------
/*!
  Klasa widoku odpowiedzialna ze okno wyboru graczy do nowej gry.
*/
class TOknoNowaGra : public TForm
{
__published:	// IDE-managed Components
        TComboBox *ComboBoxBialy;
        TComboBox *ComboBoxCzarny;
        TLabel *LabelWybierzGraczy;
        TShape *ShapeBialy;
        TShape *ShapeCzarny;
        TButton *ButtonNowaGra;
        void __fastcall ButtonNowaGraClick(TObject *Sender);
private:	// User declarations
        std::list<Gracz> gracze;
public:		// User declarations
        /*!
          Konstruktor wygenerowany automatycznie przez C++ Builder 6
        */
        __fastcall TOknoNowaGra(TComponent* Owner);
        /*!
          Metoda wczytujaca przekazana liste graczy
          do rozwijanych list wyboru graczy.

          \param g Lista graczy do wczytania
        */
        void wczytajGraczy(std::list<Gracz> g);
};
//---------------------------------------------------------------------------
extern PACKAGE TOknoNowaGra *OknoNowaGra;
//---------------------------------------------------------------------------
#endif
