//---------------------------------------------------------------------------
#ifndef GraH
#define GraH
#include "Pole.h"
#include "Kolor.h"
#include "Gracz.h"
#include "Figura.h"
#include "Pion.h"
#include "Krol.h"
#include "Krolowa.h"
#include "Goniec.h"
#include "Skoczek.h"
#include "Wieza.h"
#include <string>
#include <map>
//---------------------------------------------------------------------------
/*!
  Klasa odpowiedzialna za zarzadzanie stanem gry
*/
class Gra
{
private:
  std::map<Pole,Figura*> plansza;
  std::map<Kolor,Gracz> gracze;
  Kolor kogoRuch;
public:

  /*!
    Metoda tworzaca nowa gre dla podanych graczy

    \param gracz1 Gracz kontrolujacy biale figury
    \param gracz2 Gracz kontrolujacy czarne figury
  */
  void utworzGre(Gracz gracz1, Gracz gracz2);

  /*!
    Metoda wczytujaca stan gry z pliku

    \param nazwaPliku Nazwa pliku

    \return TRUE jezeli gra zostala wczytana
  */
  bool wczytajGre(std::string nazwaPliku);

  /*!
    Metoda zapisujaca aktualny stan gry do pliku

    \param nazwaPliku Nazwa pliku

    \return TRUE jezeli gra zostala zapisana
  */
  bool zapiszGre(std::string nazwaPliku);

  /*!
    Metoda zwracajaca informacje o figurach na planszy i ich polozeniu

    \return Stan szachownicy
  */
  std::map<Pole,Figura*> pobierzPlansze();

  /*!
    Metoda zwracajaca informacje o graczach uczestniczacych w danej partii

    \return Gracze z przypisanym kolorem figur
  */
  std::map<Kolor,Gracz> pobierzGraczy();

  /*!
    Metoda sprawdzajaca ktorego koloru figura moze teraz sie poruszac

    \return Kolor figury ktora moze sie poruszyc
  */
  Kolor sprawdzKtoRusza();

  /*!
    Metoda przesuwajaca figure z jednego pola na drugie

    \param skad Pole z ktorego figura jest przesuwana
    \param dokad Pole na ktore figura jest przesuwana
    \param trybTestowy Czy metoda ma korzystac z planszy testowej czy oryginalnej

    \return TRUE jezeli pomyslnie przesunieto figure
  */
  bool przesunFigure(Pole skad, Pole dokad, bool trybTestowy = false);

  /*!
    Metoda sprawdzajaca czy krol podanego koloru jest szachowany

    \param kolor Kolor sprawdzanego krola
    \param planszaTestowa Wskaznik do planszy testowej (jezeli metoda ma z niej skorzystac zamiast oryginalnej)

    \return TRUE jezeli jest szach
  */
  bool sprawdzCzySzach(Kolor kolor, std::map<Pole,Figura*> *planszaTestowa = 0);

  /*!
    Metoda sprawdzajaca czy krol podanego koloru jest matowany

    \param kolor Kolor sprawdzanego krola

    \return TRUE jezeli jest mat
  */
  bool sprawdzCzyMat(Kolor kolor);
};
//---------------------------------------------------------------------------
#endif
