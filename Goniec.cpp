//---------------------------------------------------------------------------
#pragma hdrstop
#include "Goniec.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
Goniec::Goniec(Kolor k)
{
  kolor = k;
}
//---------------------------------------------------------------------------
bool Goniec::walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza)
{
  if ( std::abs(skad.x - dokad.x) == std::abs(skad.y - dokad.y) )
  {
    if ( (skad.y < dokad.y) && (skad.x < dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x + i, skad.y + i)) != plansza.end() )
          return false;
    }
    else if ( (skad.y > dokad.y) && (skad.x < dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x + i, skad.y - i)) != plansza.end() )
          return false;
    }
    else if ( (skad.y > dokad.y) && (skad.x > dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x - i, skad.y - i)) != plansza.end() )
          return false;
    }
    else if ( (skad.y < dokad.y) && (skad.x > dokad.x) )
    {
      for ( int i = 1; i < std::abs(skad.x - dokad.x); i++ )
        if ( plansza.find(Pole(skad.x - i, skad.y + i)) != plansza.end() )
          return false;
    }
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------
std::string Goniec::przedstawSie()
{
  return "Goniec";
}
//---------------------------------------------------------------------------
