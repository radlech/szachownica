//---------------------------------------------------------------------------
#ifndef SkoczekH
#define SkoczekH
#include "Pole.h"
#include "Kolor.h"
#include "Figura.h"
//---------------------------------------------------------------------------
/*!
  Klasa dziedziczaca po klasie Figura odpowiedzialna za figure o nazwie skoczek.
*/
class Skoczek : public Figura
{
public:

  /*!
    Konstruktor

    \param k Kolor figury
  */
  Skoczek(Kolor k);

  /*!
    Metoda sprawdzajaca czy podany ruch jest dopuszczalny dla skoczka

    \param skad Pole na ktorym stoi figura
    \param dokad Pole na ktore figura ma byc przesunieta
    \param plansza Aktualny stan szachownicy

    \return TRUE jezeli ruch jest dopuszczalny
  */
  bool walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza);

  /*!
    Metoda zwracajaca nazwe figury

    \return Nazwa figury
  */
  std::string przedstawSie();
};
//---------------------------------------------------------------------------
#endif
