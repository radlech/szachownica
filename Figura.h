//---------------------------------------------------------------------------
#ifndef FiguraH
#define FiguraH
#include "Pole.h"
#include "Kolor.h"
#include <string>
#include <map>
//---------------------------------------------------------------------------
/*!
  Klasa abstrakcyjna odpowiedzialna za figury szachowe.
*/
class Figura
{
protected:
  Kolor kolor;
public:

  /*!
    Metoda sprawdzajaca czy podany ruch dla konkretnej figury jest dopuszczalny

    \param skad Pole na ktorym stoi figura
    \param dokad Pole na ktore figura ma byc przesunieta
    \param plansza Aktualny stan szachownicy

    \return TRUE jezeli ruch jest dopuszczalny
  */
  virtual bool walidujRuch(Pole skad, Pole dokad, std::map<Pole,Figura*> plansza);

  /*!
    Metoda zwracajaca nazwe figury

    \return Nazwa figury
  */
  virtual std::string przedstawSie();

  /*!
    Metoda ustawiajaca kolor figury

    \param k Kolor figury
  */
  void ustawKolor(Kolor k);

  /*!
    Metoda zwracajaca kolor figury

    \return Kolor figury
  */
  Kolor sprawdzKolor();
};
//---------------------------------------------------------------------------
#endif
